<?php

namespace Drupal\simple_usercentrics_cmp\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class used to store the cookies config settings.
 */
class CookiesConsentSettingsForm extends ConfigFormBase {

  /**
   * Config variable {@var string Config settings}.
   */
  const SETTINGS = 'simple_usercentrics_cmp.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'simple_usercentrics_cmp_settings_id';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config(static::SETTINGS);

    $form['is_enable'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enable Cookie consent on site.'),
      '#default_value' => $config->get('is_enable') ? $config->get('is_enable') : 0,
    ];
    $form['preview_mode'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enable Preview mode(Used for QA).'),
      '#default_value' => $config->get('preview_mode') ? $config->get('preview_mode') : 0,
    ];
    $form['script_src'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Script SRC'),
      '#default_value' => $config->get('script_src') ? $config->get('script_src') : 'https://app.usercentrics.eu/browser-ui/latest/loader.js',
    ];

    $form['usercentrics_data_settings_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('data settings id'),
      '#default_value' => $config->get('usercentrics_data_settings_id'),
      '#size' => 60,
      '#required'      => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save Settings'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $config = $this->configFactory->getEditable(static::SETTINGS);

    // Set the submitted configuration setting.
    $config
      ->set('is_enable', $form_state->getValue('is_enable'))
      ->set('preview_mode', $form_state->getValue('preview_mode'))
      ->set('script_src', $form_state->getValue('script_src'))
      ->set('usercentrics_data_settings_id', $form_state->getValue('usercentrics_data_settings_id'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
